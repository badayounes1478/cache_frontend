import React from 'react'
import '../PagesCss/Connectaccount.css'
import constant from '../../Constant'
import { NavLink } from 'react-router-dom'

const Connectaccount = (props) => {
    return (
        <div className="account-connect-container">
            <h1>Cache</h1>
            <h2>Connect Your Account</h2>
            <a href={`${constant.url}facebook/${props.match.params.id}/login`}>Facebook</a>
            <a href={`${constant.url}linkedin/${props.match.params.id}/login`}>Linked In</a>
            <a style={{ display: 'none' }} href="https://cache.yogved.in/cache/pinterest/login">Pinterest</a>
            <a href={`${constant.url}twitter/${props.match.params.id}/login`}>Twitter</a>
            <NavLink to={`/accountes/${props.match.params.id}`}>Continue</NavLink>
        </div>
    )
}

export default Connectaccount
