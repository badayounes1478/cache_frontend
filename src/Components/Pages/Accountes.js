import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import '../PagesCss/Accountes.css'
import constant from '../../Constant'

const Accountes = (props) => {
    const [twitterAccountes, settwitterAccountes] = useState([])
    const [linkedinAccountes, setlinkedinAccountes] = useState([])

    useEffect(() => {
        Axios.get(constant.url + 'twitter/accountes/' + props.match.params.id).then(res => {
            settwitterAccountes(res.data.data)
        })

        Axios.get(constant.url + 'linkedin/accountes/' + props.match.params.id).then(res => {
            setlinkedinAccountes(res.data.data)
        })

    }, [props.match.params.id])


    const getId = (id) => {
        props.history.push({
            pathname: '/cache/post/twitter',
            state: { id: id, userId: props.match.params.id}
        })
    }

    const getLinkedInId = (id) => {
        props.history.push({
            pathname: '/cache/post/linkedin',
            state: { id: id , userId:props.match.params.id}
        })
    }

    return (
        <div>
            <div style={{ padding: 50 }}>
                <h2>Twitter Accountes</h2>
                {
                    twitterAccountes.map(data => {
                        return (
                            <div key={data._id} className="accounts" onClick={() => getId(data._id)}>
                                {data.name}
                            </div>
                        )
                    })
                }
                <h2 style={{ marginTop: 10 }}>LinkedIn Accountes</h2>
                {
                    linkedinAccountes.map(data => {
                        return (
                            <div key={data._id} className="accounts" onClick={() => getLinkedInId(data._id)}>
                                {data.name}
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Accountes
