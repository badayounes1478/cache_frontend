import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import constant from '../../Constant'

const Linkedin = (props) => {

    const [status, setstatus] = useState("")
    const [file, setfile] = useState(null)
    const [date, setdate] = useState("")
    const [currentDate, setcurrentDate] = useState(null)
    const [captions, setcaptions] = useState([])
    const [templates, settemplates] = useState([])

    useEffect(() => {
        let d = new Date()
        d.setMinutes(d.getMinutes() + 5);

        let yyyy = d.getFullYear()
        let MM = d.getMonth() + 1
        if (MM < 10) {
            MM = "0" + MM
        }
        let dd = d.getDate()
        if (dd < 10) {
            dd = "0" + dd
        }
        let hh = d.getHours()
        if (hh < 10) {
            hh = "0" + hh
        }
        let mm = d.getMinutes()
        if (mm < 10) {
            mm = "0" + mm
        }

        let todayDate = `${yyyy}-${MM}-${dd}T${hh}:${mm}`
        setdate(todayDate)
        setcurrentDate(todayDate)
        getCaptions()
        getTheTemplates()
    }, [])

    const getCaptions = async () => {
        try {
            let response = await Axios.get(`https://www.captionator.creatosaurus.io/captionator/saved/${props.location.state.userId}`)
            if (response.data === "") {
                setcaptions(["Nothing saved yet"])
            } else {
                setcaptions(response.data.saved)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getTheTemplates = async () => {
        try {
            let resposne = await Axios.get('https://muse.creatosaurus.io/muse/admin/templates')
            settemplates(resposne.data)
        } catch (error) {
            console.log(error)
        }
    }

    const addStatus = () => {
        Axios.post(constant.url + 'linkedin/post', {
            id: props.location.state.id,
            status: status,
            date: date
        }).then(res => {
            console.log(res.data)
        }).catch(err => {
            console.log(err)
        })
    }

    const uploadImage = () => {
        const url = constant.url + 'linkedin/image';
        const formData = new FormData();
        formData.append('images', file)
        formData.set('status', status);
        formData.set('date', date);
        formData.set('id', props.location.state.id)

        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        Axios.post(url, formData, config).then(data => {
            console.log(data)
        }).catch(err => {
            console.log(err)
        })
    }

    return (
        <div style={{display:'flex'}}>
            <div>
                <div style={{ margin: 10, backgroundColor: '#828282', width: '60%', padding: 10, borderRadius: 5 }}>
                    <input style={{ border: 'none', background: '#dcdcdc', padding: 5, borderRadius: 5 }}
                        type="text" placeholder="enter the status" onChange={(e) => setstatus(e.target.value)} />
                    <br></br>
                    <input style={{ marginTop: 10 }} type="datetime-local" id="meeting-time" value={date} min={currentDate} onChange={(e) => setdate(e.target.value)} />
                    <br></br>
                    <button style={{ marginTop: 10 }} onClick={addStatus}>Send</button>
                </div>
                <div style={{ margin: 10, background: "#828282", padding: 10, width: '60%', borderRadius: 5 }}>
                    <h5 style={{ marginBottom: 10 }}>Upload Image</h5>
                    <input type="file" style={{ marginBottom: 10 }} id="videoUpload1" name="upload_media" accept="image/* ,video/*" onChange={(e) => setfile(e.target.files[0])} />
                    <input style={{ border: 'none', background: '#dcdcdc', padding: 5, borderRadius: 5, marginBottom: 10 }}
                        type="text" placeholder="enter the status" onChange={(e) => setstatus(e.target.value)} />
                    <br></br>
                    <input type="datetime-local" id="meeting-time" value={date} min={currentDate} onChange={(e) => setdate(e.target.value)} />
                    <br></br>
                    <button onClick={uploadImage}>Upload Image</button>
                </div>
            </div>

            <div className="caption" style={{ margin: 10 }}>
                <h2 style={{ marginBottom: 10 }}>Captionater Data</h2>
                {
                    captions.map(data => {
                        return <div className="caption-card">{data.substr(4)}</div>
                    })
                }
            </div>

            <div className="images" style={{ margin: 10 }}>
                <h2 style={{ marginBottom: 10 }}>Muse Images</h2>
                {
                    templates.map(data => {
                        return <img
                            className="template-image"
                            key={data._id}
                            src={data.image_url}
                            alt="loading" />
                    })
                }
            </div>
        </div>
    )
}

export default Linkedin
