import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Connectaccount from './Components/Pages/Connectaccount'
import Accountes from './Components/Pages/Accountes'
import Twitter from './Components/Pages/Twitter'
import Linkedin from './Components/Pages/Linkedin'
import Facebook from './Components/Pages/Facebook'
import Authentication from './Components/Pages/Authentication'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Authentication} />
        <Route exact path="/cache/:id" component={Connectaccount} />
        <Route exact path="/accountes/:id" component={Accountes} />
        <Route exact path="/cache/post/twitter" component={Twitter} />
        <Route exact path="/cache/post/linkedin" component={Linkedin} />
        <Route exact path="/cache/post/facebook" component={Facebook} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
